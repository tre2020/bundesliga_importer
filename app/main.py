import subprocess
import requests
import json
import shutil
import os
from dotenv import load_dotenv
from subprocess import *
from os import chdir

# Load the .env File 
load_dotenv()

# Import the var's from .env
ZIELPFAD = os.getenv("ZIELPFAD")
API_URL = os.getenv("API_URL")
DATEI_NAME = os.getenv("DATEI_NAME")

# Entrypoint
if __name__ == "__main__":

    try:

        # Request the JSON
        print("Starting request...")
        json_req = requests.get(API_URL).json()

        print("Successfully fetched JSON from {}".format(API_URL))

        # Write 2021.json File in App-Folder
        print("Writing local .json file...")
        with open("2021.json", "w") as json_file:
            json.dump(json_req, json_file)
            json_file.close()
            print("Closed local .json file...")

            # Copy 2021.json from applicationfolder to the destination (should be the smb drive/projectfolder)
            print("Starting copy-process...")
            source = DATEI_NAME
            destination = ZIELPFAD

            shutil.copy(source,destination)
           
            print("Successfully copied {} to {}".format(DATEI_NAME, ZIELPFAD))

            chdir("D:\ADOBE\Adobe After Effects 2021\Support Files")
            
            os.system('aerender -project Y:\ARBEIT\TVK_SB_2teLiga_Tabelle_Automatisiert\zweiteLiga_automatisiert.aep -comp "GESAMTANIMATION" -OMtemplate "TESTMEE" -output D:\output\\Gesamt.mov')
            os.system('aerender -project Y:\ARBEIT\TVK_SB_2teLiga_Tabelle_Automatisiert\zweiteLiga_automatisiert.aep -comp "STAND OBERE HÄLFTE" -OMtemplate "TESTMEE" -output D:\output\\Oben.mov')
            os.system('aerender -project Y:\ARBEIT\TVK_SB_2teLiga_Tabelle_Automatisiert\zweiteLiga_automatisiert.aep -comp "STAND UNTERE HÄLFTE" -OMtemplate "TESTMEE" -output D:\output\\Unten.mov')
    
    # Handle exceptions in case of request error
    except (requests.exceptions.ConnectionError, json.decoder.JSONDecodeError):
        print("ERROR: Could not fetch JSON from {}".format(API_URL))
        exit()
    




